output "vpc_id" {
  description = "VPC id for other modules"
  value = aws_vpc.vpc.id

}

output "private_subnet" {
  description = "Private subnet id"
  value = aws_subnet.private-office.id

}
output "office_security_group_ids" {
    description = "ssm security group id"
  value = aws_security_group.ssm_security_group.id

}

output "instance_ip" {
  description = "Instance ip address"
  value       = aws_instance.instance.private_ip

}

output "route_office" {
  description = "Route table id"
  value = aws_vpc.vpc.main_route_table_id

}

output "vpc_cidr_block" {
  description = "VPC cidr block"
  value = aws_vpc.vpc.cidr_block

}
