variable "region" {
  description = "Current region office provider"
  type        = string
}

variable "vpc_cidr_block" {
  description = "CIDR block current vpc office"
  type        = string

}

variable "private_subnet_cidr" {
  default = "CIDR block private net current vpc"
  type    = string

}
variable "instance_type" {
  description = "Type of instance"
  type        = string

}

variable "instance_profile" {
  description = "Instance profile"
  type        = string

}

