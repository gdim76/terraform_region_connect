terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

# Create new VPC in region
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = "true"
  enable_dns_support   = "true"

  tags = {
    Name = "vpc-office-${var.region}"
  }

}

# Create private network in new VPC
resource "aws_subnet" "private-office" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_subnet_cidr
  availability_zone       = "${var.region}a"
  map_public_ip_on_launch = false

  tags = {
    Name = "Private-subnet-${var.region}"
  }
}

# Create security group for ssm 
resource "aws_security_group" "ssm_security_group" {
  name        = "ssm-access-sg"
  description = "Security Group for SSM Access"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = local.https_port
    to_port     = local.https_port
    protocol    = local.tcp_protocol
    cidr_blocks = [aws_vpc.vpc.cidr_block]
  }

}

# Security group for instance
resource "aws_security_group" "instance_security_group" {
  vpc_id = aws_vpc.vpc.id
  # Allowc inbond icmp traffic
  ingress {
    from_port   = local.icmp_port
    to_port     = local.icmp_port
    protocol    = local.icmp_protocol
    cidr_blocks = local.any_network
  }

  # Allowc outbond icmp and https traffic
  egress {
    from_port   = local.https_port
    to_port     = local.https_port
    protocol    = "tcp"
    cidr_blocks = local.any_network
  }
  egress {
    from_port   = local.icmp_port
    to_port     = local.icmp_port
    protocol    = local.icmp_protocol
    cidr_blocks = local.any_network
  }


  tags = {
    Name = "EC2 Instance security group"
  }
}

# Create endpoint ssm for access to instance

resource "aws_vpc_endpoint" "ssm_endpoint" {
  vpc_id              = aws_vpc.vpc.id
  for_each            = local.endpoints
  vpc_endpoint_type   = "Interface"
  service_name        = "com.amazonaws.${var.region}.${each.value.name}"
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.ssm_security_group.id]
  subnet_ids          = [aws_subnet.private-office.id]

}


# Get latest ami in central region 
data "aws_ami" "amzn-linux-2023-ami-cental" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["al2023-ami-2023.*-x86_64"]
  }
}
# Create instance

resource "aws_instance" "instance" {
  instance_type          = var.instance_type
  ami                    = data.aws_ami.amzn-linux-2023-ami-cental.id
  subnet_id              = aws_subnet.private-office.id
  iam_instance_profile   = var.instance_profile
  vpc_security_group_ids = [aws_security_group.instance_security_group.id]

  tags = {
    Name = "office-${var.region}"
  }


}


# local variables

locals {
  icmp_port     = -1
  icmp_protocol = "icmp"
  https_port    = 443
  tcp_protocol  = "tcp"
  any_network   = ["0.0.0.0/0"]
  endpoints = {
    "endpoint-ssm" = {
      name = "ssm"
    },
    "endpoint-ec2" = {
      name = "ec2"
    },
    "endpoint-ssm-messages" = {
      name = "ssmmessages"
    },
    "endpoint-ec2-messages" = {
      name = "ec2messages"
    }
  }
}


