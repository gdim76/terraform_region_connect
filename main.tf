provider "aws" {
  region                   = "eu-central-1"
  alias                    = "central"
  shared_config_files      = ["C:/Users/Admin/.aws/config"]
  shared_credentials_files = ["C:/Users/Admin/.aws/credentials"]
}

provider "aws" {
  region                   = "eu-west-1"
  alias                    = "west"
  shared_config_files      = ["C:/Users/Admin/.aws/config"]
  shared_credentials_files = ["C:/Users/Admin/.aws/credentials"]
}

# Create resources in office 1
module "vpc_office_1" {
  source              = "./module/resources"
  vpc_cidr_block      = "10.0.0.0/16"
  private_subnet_cidr = "10.0.1.0/24"
  region              = "eu-central-1"
  instance_type       = "t2.micro"
  instance_profile    = aws_iam_instance_profile.ssm_role_instance_profile.name
  providers = {
    aws = aws.central
  }

}

# Create resources in office 2
module "vpc_office_2" {
  source              = "./module/resources"
  vpc_cidr_block      = "10.1.0.0/16"
  private_subnet_cidr = "10.1.1.0/24"
  region              = "eu-west-1"
  instance_type       = "t2.micro"
  instance_profile    = aws_iam_instance_profile.ssm_role_instance_profile.name

  providers = {
    aws = aws.west
  }

}


# Create peer connection between offices 
resource "aws_vpc_peering_connection" "officeconnect" {
  #  provider = aws.central
  peer_owner_id = local.owners_id
  peer_vpc_id   = module.vpc_office_2.vpc_id
  vpc_id        = module.vpc_office_1.vpc_id
  peer_region   = "eu-west-1"
  auto_accept   = false
  #  depends_on    = [module.vpc_office_2, module.vpc_office_1]

}

# Accept peering connection
resource "aws_vpc_peering_connection_accepter" "peer" {
  provider                  = aws.west
  vpc_peering_connection_id = aws_vpc_peering_connection.officeconnect.id
  auto_accept               = true

  tags = {
    Side = "Accepter"
  }

}

# Create Route in the office's 
resource "aws_route" "route_to_peer_vpc_office_1" {
  provider                  = aws.central
  route_table_id            = module.vpc_office_1.route_office
  destination_cidr_block    = module.vpc_office_2.vpc_cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.officeconnect.id

}

resource "aws_route" "route_to_peer_vpc_office_2" {
  provider                  = aws.west
  route_table_id            = module.vpc_office_2.route_office
  destination_cidr_block    = module.vpc_office_1.vpc_cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.officeconnect.id

}




# Create IAM role access only SSM
resource "aws_iam_role" "ssm_role" {
  name = "SSM-Access-Role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        },
        Action = "sts:AssumeRole"
      }
    ]
  })

}

# Attach AmazonSSMManagedInstanceCore policy to the IAM role
resource "aws_iam_role_policy_attachment" "ec2_role_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  role       = aws_iam_role.ssm_role.name
}

# Create IAM profile for instances

resource "aws_iam_instance_profile" "ssm_role_instance_profile" {
  name = "ssm_instance_profile"
  role = aws_iam_role.ssm_role.name
}



locals {
  owners_id = "424944963246"
}
