output "instance_profile" {
  description = "Instance profile"
  value       = aws_iam_instance_profile.ssm_role_instance_profile.name

}


output "instance_ip_office_1" {
  description = "Ip addres instance office 1"
  value       = module.vpc_office_1.instance_ip

}


output "instance_ip_office_2" {
  description = "Ip addres instance office 2"
  value       = module.vpc_office_2.instance_ip

}

output "vpc_cidr_block_office-1" {
  description = "Cidr block vpc office 1"
  value       = module.vpc_office_1.vpc_cidr_block

}
output "vpc_cidr_block_office-2" {
  description = "Cidr block vpc office 2"
  value       = module.vpc_office_2.vpc_cidr_block

}
